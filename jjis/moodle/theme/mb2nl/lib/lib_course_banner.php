<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package   theme_mb2nl
 * @copyright 2017 - 2020 Mariusz Boloz (mb2themes.com)
 * @license   Commercial https://themeforest.net/licenses
 *
 */

defined('MOODLE_INTERNAL') || die();


/*
 *
 * Method to get course banner
 *
 */
function theme_mb2nl_course_banner ($page)
{

	global $CFG, $COURSE;

	$output = '';
	$bannerStyle = theme_mb2nl_theme_setting($page, 'cbannerstyle');
	$bannerTitle = theme_mb2nl_theme_setting($page, 'cbannertitle');
	$bannerUrl = theme_mb2nl_course_image_url( $COURSE->id );

	if ($COURSE->id <= 1)
	{
		return;
	}

	if ($bannerStyle === '')
	{
		return;
	}

	if ( !$bannerUrl )
	{
		return;
	}

	$bannerSelector = '<div class="cbanner-bg" style="background-image:url(\'' . $bannerUrl  . '\');"><a href="' . $CFG->wwwroot . '/course/view.php?id=' .
	$COURSE->id . '" style="display:block;"><div class="banner-bg-inner">';

	$output .= '<div class="theme-cbanner cbanner-' . $bannerStyle . '">';
	$output .= $bannerStyle === 'fw' ? $bannerSelector : '';
	$output .= '<div class="container-fluid">';
	$output .= '<div class="row">';
	$output .= '<div class="col-md-12">';
	$output .= $bannerStyle === 'fx' ? $bannerSelector : '';
	$output .= $bannerTitle ? '<h1>' . format_text( $COURSE->fullname ) . '</h1>' : '';
	$output .= '</div>';
	$output .= $bannerStyle === 'fx' ? '</a>' : '';
	$output .= '</div>';
	$output .= '</div>';
	$output .= '</div>';
	$output .= $bannerStyle === 'fw' ? '</a>' : '';
	$output .= '</div>';
	$output .= '</div>';

	return $output;

}
