::q1:: An angle whose measure is equal to 90° is
{
~Acute angle
=Right angle
~Obtuse angle
~Straight angle
}

::q2:: Fill in the blanks
An angle whose measure is less than 90° is______.
{
~Right angle
=Acute angle
~Straight angle
~Obtuse angle
}

::q3:: Fill in the blanks
An angle whose measure is greater than 90° is ______.
{
~Right angle
~Acute angle
=Straight angle
~Obtuse angle
}

::q4:: Fill in the blanks
An angle whose measure is the sum of the measures of two 90° is _____.
{
~Right angle
~Acute angle
~Straight angle
=Obtuse angle
}

::q5:: Fill in the blanks
When the sum of the measures of two angles is 90°, then each one of them is ______.
{
~Right angle
=Acute angle
~Straight angle
~Obtuse angle
}

::q6:: Fill in the blanks
What will be the other angle if the sum of the measures of two angles is that of a straight angle and one of them is acute
{
~Right angle
~Acute angle
~Straight angle
=Obtuse angle
}

::q7:: A cuboid has
{
~2 faces, 4 edges and 6 vertices.
~4 faces, 6 edges and 6 vertices.
~5 faces, 10 edges and 8 vertices.
=6 faces, 12 edges and 8 vertices.
}

::q8:: A basket ball is an example of a
{
~Cube.
~Cone.
=Sphere.
~Cylinder.
}

::q9:: A triangle with its sides measure 5 cm, 5 cm and 5 cm is
{
~Scalene triangle:
~Isosceles triangle:
=Equilateral triangle:
~Right angled triangle
}


::q10:: An angle whose measure is equal to half of a revolution is
{
~Acute angle
=Right angle
~Obtuse angle
~Straight angle
}

::q11:: An angle whose measure is equal to a full revolution is
{
~Obtuse angle
=Complete angle
~Straight angle
~Right angle
}

::q12:: An angle whose measure is equal to 90°.
{
~Obtuse angle
~Complete angle
~Straight angle
=Right angle
}

::q13:: An angle whose measure is less than 90°.
{
~Obtuse angle
=Acute angle
~Straight angle
~Right angle
}

::q14:: An angle whose measure is more than 90°.
{
~Right angle
~Acute angle
~Straight angle
=Obtuse angle
}

::q15:: Fill in the blanks
There are __________ main directions.
{
~4
=3
~2
~1
}

::q16:: Fill in the blanks
A line segment is a fixed portion of a _________.
{
=Line
~Circle
~Angle
~None of these
}

::q17:: Fill in the blanks
The angle for one revolution is a __________________.
{
~Acute angle
~Right angle
=Complete angle
~None of these
}

::q18:: Fill in the blanks
A ________ angle is larger than a straight angle.
{
~Acute angle
~Right angle
=Reflex angle
~Obtuse angle
}

::q19:: In how many parts does a complete revolution divide?
{
~90
~180
~270
=360
}

::q20:: Where will the hand of a clock stop if it starts at 12 and makes half of a revolution, clockwise?
{
~3
~4
=6
~7
}

















































































