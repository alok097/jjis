::1:: _______ facilitates reabsorption of water by the nephron.
{
=Loop of the nephron
~Medulla
~Cortex
~Pelvis
}

::1:: A Brush border is formed in
{
~Distal convoluted tubule
=Proximal convoluted tubule
~Bowman’s capsule
~Loop of Henle
}

::1:: Ability of the kidneys for the production of concentrated urine is dependent on _______.
{
~Active transport
~Passive transport
=Countercurrent mechanism
~Diffusion
}

::1:: Accumulation of urea and other waste substances in the blood is called
{
~Hemodialysis
~Cystitis
=Uremia
~Urethritis
}

::1:: Bile manufactured by liver is stored within the ________.
{
~Urinary bladder
=Gallbladder
~Liver
~Lungs
}

::1:: Certain ions and molecules, for examples H+ and penicillin are secreted from the peritubular capillary network into the _______.
{
=Convoluted tubules
~Peritubular capillaries
~Loops of Henle
~Collecting ducts
}
