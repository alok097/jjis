<?php

class __Mustache_cb5930dcb784f415acdd8d86a2933a7c extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '<div>
';
        // 'msjgroup' section
        $value = $context->find('msjgroup');
        $buffer .= $this->sectionD50f52fe83d99a240c837da1010da3f3($context, $indent, $value);
        $buffer .= $indent . '    <div id="bigbluebuttonbn-mobile-notifications" style="display:none; color:#31708f; background-color: #d9edf7; padding-right: 35px; border-color: #bce8f1; padding: 15px; margin-bottom: 20px;">
';
        $buffer .= $indent . '    {{ \'plugin.mod_bigbluebuttonbn.view_mobile_message_reload_page_creation_time_meeting\' | translate }}
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '    <core-course-module-description description="';
        $value = $this->resolveValue($context->findDot('bigbluebuttonbn.intro'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '" component="mod_bigbluebuttonbn" componentId="';
        $value = $this->resolveValue($context->find('cmid'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '"></core-course-module-description>
';
        $buffer .= $indent . '    <ion-list>
';
        // 'errors' section
        $value = $context->find('errors');
        $buffer .= $this->sectionEad4d4265607005d559b595352de664e($context, $indent, $value);
        // 'errors' inverted section
        $value = $context->find('errors');
        if (empty($value)) {
            
            $buffer .= $indent . '            <ion-item text-wrap id="bigbluebuttonbn-mobile-meetingready">
';
            $buffer .= $indent . '                {{ \'plugin.mod_bigbluebuttonbn.view_message_conference_room_ready\' | translate }}
';
            $buffer .= $indent . '            </ion-item>
';
            $buffer .= $indent . '            <ion-item>
';
            $buffer .= $indent . '                <button id="bigbluebuttonbn-mobile-join" ion-button block onclick="window.open(\'';
            $value = $this->resolveValue($context->find('urltojoin'), $context);
            $buffer .= call_user_func($this->mustache->getEscape(), $value);
            $buffer .= '\', \'_system\');">
';
            $buffer .= $indent . '                    {{ \'plugin.mod_bigbluebuttonbn.view_conference_action_join\' | translate }}
';
            $buffer .= $indent . '                </button>
';
            $buffer .= $indent . '            </ion-item>
';
        }
        $buffer .= $indent . '    </ion-list>
';
        $buffer .= $indent . '</div>
';

        return $buffer;
    }

    private function sectionD50f52fe83d99a240c837da1010da3f3(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
        <div style="color:#856404; background-color: #fff3cd; padding-right: 35px; border-color: #ffeeba; padding: 15px; margin-bottom: 20px;">
        <% message %>
        </div>
    ';
            $result = call_user_func($value, $source, $this->lambdaHelper->withDelimiters('{{= <% %> =}}'));
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result, '{{= <% %> =}}')
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '        <div style="color:#856404; background-color: #fff3cd; padding-right: 35px; border-color: #ffeeba; padding: 15px; margin-bottom: 20px;">
';
                $buffer .= $indent . '        ';
                $value = $this->resolveValue($context->find('message'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '
';
                $buffer .= $indent . '        </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionEad4d4265607005d559b595352de664e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            <ion-item text-wrap>
                <% error %>
            </ion-item>
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper->withDelimiters('{{= <% %> =}}'));
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result, '{{= <% %> =}}')
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            <ion-item text-wrap>
';
                $buffer .= $indent . '                ';
                $value = $this->resolveValue($context->find('error'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '
';
                $buffer .= $indent . '            </ion-item>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
