<?php

class __Mustache_c2228c33a065cc996808a82446b5fc5c extends Mustache_Template
{
    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $buffer = '';

        $buffer .= $indent . '<div style="padding: 1em">
';
        $buffer .= $indent . '    <div>Iframe embedding must be enabled in order to display H5P content in the mobile app.</div>
';
        $buffer .= $indent . '    <div>You can enable it by checking "Allow frame embedding" in <a href="/admin/settings.php?section=httpsecurity" core-link>Site Administration / Security / Http Security</a></div>
';
        $buffer .= $indent . '</div>';

        return $buffer;
    }
}
