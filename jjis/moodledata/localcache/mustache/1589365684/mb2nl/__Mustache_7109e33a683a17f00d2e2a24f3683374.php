<?php

class __Mustache_7109e33a683a17f00d2e2a24f3683374 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '<!--/**
';
        $buffer .= $indent . ' * *************************************************************************
';
        $buffer .= $indent . ' * *                         OOHOO - Tab Display                          **
';
        $buffer .= $indent . ' * *************************************************************************
';
        $buffer .= $indent . ' * @package     mod                                                       **
';
        $buffer .= $indent . ' * @subpackage  tab                                                       **
';
        $buffer .= $indent . ' * @name        tab                                                       **
';
        $buffer .= $indent . ' * @copyright   oohoo.biz                                                 **
';
        $buffer .= $indent . ' * @link        http://oohoo.biz                                          **
';
        $buffer .= $indent . ' * @author      Patrick Thibaudeau                                        **
';
        $buffer .= $indent . ' * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
';
        $buffer .= $indent . ' * *************************************************************************
';
        $buffer .= $indent . ' * ************************************************************************ */-->
';
        // 'intro' section
        $value = $context->find('intro');
        $buffer .= $this->section9f9437a226e958f26be73651f0412e02($context, $indent, $value);
        $buffer .= $indent . '<div class="row">
';
        // 'showMenu' section
        $value = $context->find('showMenu');
        $buffer .= $this->sectionE15da06978f59b75870e28d3134964fb($context, $indent, $value);
        $buffer .= $indent . '    <div class="col';
        // 'showMenu' section
        $value = $context->find('showMenu');
        $buffer .= $this->section2d7846686663ccc1716d992ce88aaf54($context, $indent, $value);
        $buffer .= '">
';
        $buffer .= $indent . '        <ul class="nav nav-tabs" id="TabbedPanelsTabGroup" role="tablist">
';
        // 'tabs' section
        $value = $context->find('tabs');
        $buffer .= $this->sectionF498de294b1d3db5efdbb02a8e4bd248($context, $indent, $value);
        $buffer .= $indent . '        </ul>
';
        $buffer .= $indent . '        <div class="tab-content" id="TabbedPanelsTabContent">
';
        // 'tabs' section
        $value = $context->find('tabs');
        $buffer .= $this->sectionD3c57ee3a7a5825151de0381f3a387f3($context, $indent, $value);
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '</div>
';

        return $buffer;
    }

    private function section9f9437a226e958f26be73651f0412e02(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
<div id="mod_tab_introbox" class="row">
    <div class="col mb-3">
        {{{intro}}}
    </div>
</div>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '<div id="mod_tab_introbox" class="row">
';
                $buffer .= $indent . '    <div class="col mb-3">
';
                $buffer .= $indent . '        ';
                $value = $this->resolveValue($context->find('intro'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '    </div>
';
                $buffer .= $indent . '</div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section1158bd675ebbb8a2782b4b3b1b661328(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                    <li class="list-group-item">
                        <a href="view.php?id={{{id}}}" title="{{{name}}}">{{{name}}}</a>
                    </li>
                    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                    <li class="list-group-item">
';
                $buffer .= $indent . '                        <a href="view.php?id=';
                $value = $this->resolveValue($context->find('id'), $context);
                $buffer .= $value;
                $buffer .= '" title="';
                $value = $this->resolveValue($context->find('name'), $context);
                $buffer .= $value;
                $buffer .= '">';
                $value = $this->resolveValue($context->find('name'), $context);
                $buffer .= $value;
                $buffer .= '</a>
';
                $buffer .= $indent . '                    </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section89fe9ec1cae85c35923c054417ff8fd9(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    <div class="col-md-3">
        <div class="card">
            <div class="card-header">
                <h4>{{{name}}}</h4>
            </div>
            <div class="card-body">                
                <ul class="list-group">
                    {{#items}}
                    <li class="list-group-item">
                        <a href="view.php?id={{{id}}}" title="{{{name}}}">{{{name}}}</a>
                    </li>
                    {{/items}}
                </ul>
            </div>
        </div>
    </div>
    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '    <div class="col-md-3">
';
                $buffer .= $indent . '        <div class="card">
';
                $buffer .= $indent . '            <div class="card-header">
';
                $buffer .= $indent . '                <h4>';
                $value = $this->resolveValue($context->find('name'), $context);
                $buffer .= $value;
                $buffer .= '</h4>
';
                $buffer .= $indent . '            </div>
';
                $buffer .= $indent . '            <div class="card-body">                
';
                $buffer .= $indent . '                <ul class="list-group">
';
                // 'items' section
                $value = $context->find('items');
                $buffer .= $this->section1158bd675ebbb8a2782b4b3b1b661328($context, $indent, $value);
                $buffer .= $indent . '                </ul>
';
                $buffer .= $indent . '            </div>
';
                $buffer .= $indent . '        </div>
';
                $buffer .= $indent . '    </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionE15da06978f59b75870e28d3134964fb(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    {{#menu}}
    <div class="col-md-3">
        <div class="card">
            <div class="card-header">
                <h4>{{{name}}}</h4>
            </div>
            <div class="card-body">                
                <ul class="list-group">
                    {{#items}}
                    <li class="list-group-item">
                        <a href="view.php?id={{{id}}}" title="{{{name}}}">{{{name}}}</a>
                    </li>
                    {{/items}}
                </ul>
            </div>
        </div>
    </div>
    {{/menu}}
    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                // 'menu' section
                $value = $context->find('menu');
                $buffer .= $this->section89fe9ec1cae85c35923c054417ff8fd9($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section2d7846686663ccc1716d992ce88aaf54(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '-md-9';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= '-md-9';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section5749c750acb0d7477dd5257d00cc6d53(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'active';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'active';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionF498de294b1d3db5efdbb02a8e4bd248(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            <li class="nav-item">
                <a class="nav-link {{#active}}active{{/active}}" id="tab-{{{id}}}" data-toggle="tab" href="#tab{{{id}}}" role="tab" aria-controls="tab{{{id}}}">{{{name}}}</a>
            </li>
            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            <li class="nav-item">
';
                $buffer .= $indent . '                <a class="nav-link ';
                // 'active' section
                $value = $context->find('active');
                $buffer .= $this->section5749c750acb0d7477dd5257d00cc6d53($context, $indent, $value);
                $buffer .= '" id="tab-';
                $value = $this->resolveValue($context->find('id'), $context);
                $buffer .= $value;
                $buffer .= '" data-toggle="tab" href="#tab';
                $value = $this->resolveValue($context->find('id'), $context);
                $buffer .= $value;
                $buffer .= '" role="tab" aria-controls="tab';
                $value = $this->resolveValue($context->find('id'), $context);
                $buffer .= $value;
                $buffer .= '">';
                $value = $this->resolveValue($context->find('name'), $context);
                $buffer .= $value;
                $buffer .= '</a>
';
                $buffer .= $indent . '            </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionD3c57ee3a7a5825151de0381f3a387f3(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            <div class="tab-pane fade show {{#active}}active{{/active}} mt-3" id="tab{{{id}}}" role="tabpanel" aria-labelledby="tab{{{id}}}">
                {{{content}}}
            </div>
            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            <div class="tab-pane fade show ';
                // 'active' section
                $value = $context->find('active');
                $buffer .= $this->section5749c750acb0d7477dd5257d00cc6d53($context, $indent, $value);
                $buffer .= ' mt-3" id="tab';
                $value = $this->resolveValue($context->find('id'), $context);
                $buffer .= $value;
                $buffer .= '" role="tabpanel" aria-labelledby="tab';
                $value = $this->resolveValue($context->find('id'), $context);
                $buffer .= $value;
                $buffer .= '">
';
                $buffer .= $indent . '                ';
                $value = $this->resolveValue($context->find('content'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '            </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
